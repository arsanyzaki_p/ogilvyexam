﻿using System.Web.Mvc;
using BankCheque.Models.Cheque;
using BankCheque.BusinessModels;
namespace BankCheque.Controllers
{
    public class ChequeController : Controller
    {
        [HttpGet]
        public ActionResult CollectChequeData()
        {
            return View("~/Views/Cheque/ChequeDataCollection.cshtml", new ChequeVM()
            {
                PageTitle = "Create a Bank Cheque",
                HelpMessage = "Cheque details",
                ChequeValueLabel = "Cheque Value",
                PayeeNameLabel = "Payee Name",
                SubmitButtonLabel = "Submit"
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult SubmitCheque(Cheque cheque)
        {
            cheque.ValidateChequeParameters();

            // Do Some back end operations

            return View("~/Views/Cheque/SubmittedCheque.cshtml", new ChequeVM()
            {
                PageTitle = "Bank Cheque",
                ChequeData = new Cheque()
                {
                    PayeeName = cheque.PayeeName,
                    ChequeValueDollars = cheque.ChequeValueDollars,
                    ChequeValueCents = cheque.ChequeValueCents
                }
            });
        }
        
    }
    
}