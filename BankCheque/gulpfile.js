﻿var gulp = require('gulp');
var minify = require('gulp-minify');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');

gulp.task('default', done => {
    gulp.src(['./Content/site.css'])
        .pipe(minify())
        .pipe(gulp.dest('./Content'));
    gulp.src('./Content/site.css')
        .pipe(cssmin())
        .pipe(rename({ suffix: '-min' }))
        .pipe(gulp.dest('./Content'));
    done();
});

