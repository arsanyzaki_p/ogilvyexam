﻿namespace BankCheque.Models.Cheque
{
    public class ChequeVM
    {
        public BusinessModels.Cheque ChequeData = new BusinessModels.Cheque();
        public string PageTitle { get; set; }
        public string HelpMessage { get; set; }
        public string SubmitButtonLabel { get; set; }
        public string PayeeNameLabel { get; set; }
        public string ChequeValueLabel { get; set; }

    }
}