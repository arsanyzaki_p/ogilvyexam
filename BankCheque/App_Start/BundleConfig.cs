﻿using System.Web.Optimization;

namespace BankCheque
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/script/jquery").Include(
                "~/Scripts/jquery-1.10.2.min.js"));

            bundles.Add(new ScriptBundle("~/script/jqueryval").Include(
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new ScriptBundle("~/script/chequescript").Include(
                "~/Scripts/cheque-min.js"));

            bundles.Add(new ScriptBundle("~/script/bootstrap").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/respond.min.js"));

            bundles.Add(new StyleBundle("~/style/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/site-min.css"));
        }
    }
}
