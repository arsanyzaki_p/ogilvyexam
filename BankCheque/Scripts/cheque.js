
Cheque.prototype.AddValidationMsgs = function (validationMsgs) {
    var elements = [];
    elements[0] = document.getElementById("cheque-payee-name");
    elements[1] = document.getElementById("cheque-dollars-value");
    elements[2] = document.getElementById("cheque-cents-value");

    for (i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                if (e.target.validity.valueMissing) {
                    e.target.setCustomValidity(validationMsgs.RequiredField);
                }
                else if (e.target.validity.patternMismatch) {
                    if (e.target.value.trim() == "") {
                        e.target.setCustomValidity(validationMsgs.FieldNotBlank);
                    }
                    else {
                        if (e.target.id == "cheque-payee-name") {
                            e.target.setCustomValidity(validationMsgs.NoBlankOrSymbols);
                        }
                        if (e.target.id == "cheque-dollars-value") {
                            e.target.setCustomValidity(validationMsgs.DigitsOnlyDollarsValue);
                        }
                        if (e.target.id == "cheque-cents-value") {
                            e.target.setCustomValidity(validationMsgs.DigitsOnlyCentsValue);
                        }
                    }
                }
                else {
                    e.target.setCustomValidity(validationMsgs.InvalidInput);
                }
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
};



function Cheque() {
    var validationMsgs = {
        RequiredField: "This field is required",
        FieldNotBlank: "This field can't be blank",
        NoBlankOrSymbols: "Name can't contain numbers or special characters",
        DigitsOnlyDollarsValue: "Digits only are allowed with maximum of 100 trillion",
        DigitsOnlyCentsValue: "Digits only are allowed with maximum of 2 digits",
        InvalidInput: "Invalid input"
    }
    this.AddValidationMsgs(validationMsgs);
}

document.addEventListener("DOMContentLoaded", function () {
    new Cheque();
});