﻿using System;
using System.Globalization;
using BankCheque.Utilities;

namespace BankCheque.BusinessModels
{
    public class Cheque
    {
        private long _ChequeValueDollars;
        public long ChequeValueDollars
        {
            get
            {
                return _ChequeValueDollars;
            }
            set
            {
                _ChequeValueDollars = value;
            }
        }

        private int _ChequeValueCents;
        public int ChequeValueCents
        {
            get
            {
                return _ChequeValueCents;
            }
            set
            {
                _ChequeValueCents = value;
            }
        }

        private string _PayeeName;
        public string PayeeName
        {
            get
            {
                return _PayeeName;
            }
            set
            {
                _PayeeName = value;
            }
        }

        public string getValueInWords()
        {
            return CurrencyUtilities.ConvertCurrencyToWords(ChequeValueDollars, ChequeValueCents);
        }

        public string GetFormatedValue()
        {
            return TotalValue.ToString("C", CultureInfo.CurrentCulture);
        }

        public void ValidateChequeParameters()
        {
            ValidateChequePayeeName();
            ValidateChequeValue();
        }

        private void ValidateChequeValue()
        {
            if (TotalValue <= 0)
            {
                throw new Exception("Cheque value can't be negative");
            }
            if (TotalValue > 100000000000000)
            {
                throw new Exception("Cheque value can't be above 100 trillion");
            }
            if (ChequeValueCents > 99)
            {
                throw new Exception("Cents value can't be above 99");
            }
        }

        private void ValidateChequePayeeName()
        {
            if (string.IsNullOrWhiteSpace(PayeeName.Trim()))
            {
                throw new Exception("Name can't be null, empty or white space");
            }
            if (!StringUtilities.IsLettersAndSpacesOnly(PayeeName.Trim()))
            {
                throw new Exception("Name can contain letters and spaces only");
            }
        }

        private double TotalValue
        {
            get
            {
                return ChequeValueDollars + (ChequeValueCents / 100.0);
            }
        }

    }
}