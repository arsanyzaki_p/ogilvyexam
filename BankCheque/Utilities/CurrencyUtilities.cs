﻿using Humanizer;
namespace BankCheque.Utilities
{
    static public class CurrencyUtilities
    {
        static public string ConvertCurrencyToWords(long dollars, int cents)
        {
            var valueInWords = $"{(dollars).ToWords()} DOLLARS AND {(cents.ToWords())} CENTS".ToUpper();
            return valueInWords
                .Replace("THOUSAND", "THOUSAND,")
                .Replace("MILLION", "MILLION,")
                .Replace("BILLION", "BILLION,")
                .Replace("TRILLION", "TRILLION,")
                .Replace(", DOLLARS", " DOLLARS");
        }
    }
}