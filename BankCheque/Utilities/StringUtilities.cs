﻿using System.Text.RegularExpressions;
namespace BankCheque.Utilities
{
    static public class StringUtilities
    {
        static public bool IsLettersAndSpacesOnly(string str)
        {
            if (Regex.IsMatch(str, @"^[a-zA-Z ]+$"))
            {
                return true;
            }
            return false;
        }
    }
}