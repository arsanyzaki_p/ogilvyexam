﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankCheque.BusinessModels;

namespace BankCheque.Tests.Controllers
{

    public class ChequeTestModel
    {
        public long DollarsValue;
        public int Cents;
        public string FormattedValue;
        public string ValueInWords;
        public string PayeeName;
    }

    [TestClass]
    public class ChequeTest
    {
        List<ChequeTestModel> chequeTestModelList_ValidValues;
        List<Cheque> chequeList_ValidNames;
        public ChequeTest()
        {
            chequeTestModelList_ValidValues = new List<ChequeTestModel>()
            {
                new ChequeTestModel()
                {
                    DollarsValue = 123,
                    Cents = 12,
                    FormattedValue = @"$123.12",
                    ValueInWords = @"ONE HUNDRED AND TWENTY-THREE DOLLARS AND TWELVE CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 1200,
                    Cents = 0,
                    FormattedValue = @"$1,200.00",
                    ValueInWords = @"ONE THOUSAND, TWO HUNDRED DOLLARS AND ZERO CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 1,
                    Cents = 0,
                    FormattedValue = @"$1.00",
                    ValueInWords = @"ONE DOLLARS AND ZERO CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 200,
                    Cents = 0,
                    FormattedValue = @"$200.00",
                    ValueInWords = @"TWO HUNDRED DOLLARS AND ZERO CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 5222,
                    Cents = 5,
                    FormattedValue = @"$5,222.05",
                    ValueInWords = @"FIVE THOUSAND, TWO HUNDRED AND TWENTY-TWO DOLLARS AND FIVE CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 1200300,
                    Cents = 0,
                    FormattedValue = @"$1,200,300.00",
                    ValueInWords = @"ONE MILLION, TWO HUNDRED THOUSAND, THREE HUNDRED DOLLARS AND ZERO CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 999999999999,
                    Cents = 99,
                    FormattedValue = @"$999,999,999,999.99",
                    ValueInWords = @"NINE HUNDRED AND NINETY-NINE BILLION, NINE HUNDRED AND NINETY-NINE MILLION, NINE HUNDRED AND NINETY-NINE THOUSAND, NINE HUNDRED AND NINETY-NINE DOLLARS AND NINETY-NINE CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 100000000000000,
                    Cents = 0,
                    FormattedValue = @"$100,000,000,000,000.00",
                    ValueInWords = @"ONE HUNDRED TRILLION DOLLARS AND ZERO CENTS"
                },
                new ChequeTestModel()
                {
                    DollarsValue = 7000000000000,
                    Cents = 0,
                    FormattedValue = @"$7,000,000,000,000.00",
                    ValueInWords = @"SEVEN TRILLION DOLLARS AND ZERO CENTS"
                }
            };

            chequeList_ValidNames = new List<Cheque>()
            {
                new Cheque()
                {
                   PayeeName = "Paul Black Miller",
                   ChequeValueDollars = 1,
                   ChequeValueCents = 0 
                },
                new Cheque()
                {
                   PayeeName = "Sandra  Sam ",
                   ChequeValueDollars = 1,
                   ChequeValueCents = 0
                },
                new Cheque()
                {
                   PayeeName = "  Simon N Peter",
                   ChequeValueDollars = 1,
                   ChequeValueCents = 0
                },
                new Cheque()
                {
                   PayeeName = "Nancy GREEN",
                   ChequeValueDollars = 1,
                   ChequeValueCents = 0
                }
            };
        }

        [TestMethod]
        public void Test_getValueInWords_GetFormatedValue()
        {

            foreach(var chequeTest in chequeTestModelList_ValidValues)
            {
                string name = "Paul David";
                var cheque = new Cheque()
                {
                    ChequeValueDollars = chequeTest.DollarsValue,
                    ChequeValueCents = chequeTest.Cents,
                    PayeeName = name
                };

                Assert.AreEqual(chequeTest.FormattedValue, cheque.GetFormatedValue());
                Assert.AreEqual(chequeTest.ValueInWords, cheque.getValueInWords());
            }
        }

        [TestMethod]
        public void Test_ValidateChequeParameters_ValidName()
        {
            foreach (var cheque in chequeList_ValidNames)
            {
                cheque.ValidateChequeParameters();
            }
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void Test_ValidateChequeParameters_EmptyName()
        {
            (new Cheque()
            {
                ChequeValueDollars = 1,
                ChequeValueCents = 0,
                PayeeName = string.Empty
            }).ValidateChequeParameters();
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void Test_ValidateChequeParameters_WhiteSpaceName()
        {
            (new Cheque()
            {
                ChequeValueDollars = 1,
                ChequeValueCents = 0,
                PayeeName = "   "
            }).ValidateChequeParameters();
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void Test_ValidateChequeParameters_SymbolName()
        {
            (new Cheque()
            {
                ChequeValueDollars = 1,
                ChequeValueCents = 0,
                PayeeName = "Paul %"
            }).ValidateChequeParameters();
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void Test_ValidateChequeParameters_CentsMoreThanThreeDigits()
        {
            (new Cheque()
            {
                ChequeValueDollars = 1,
                ChequeValueCents = 111,
                PayeeName = "Paul"
            }).ValidateChequeParameters();
        }

        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void Test_ValidateChequeParameters_NegativeDollarsValue()
        {
            (new Cheque()
            {
                ChequeValueDollars = -1,
                ChequeValueCents = 0,
                PayeeName = "Paul"
            }).ValidateChequeParameters();
        }
    }
}
